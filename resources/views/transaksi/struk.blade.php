<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Cetak Struk</title>
  </head>
  <body onload="window.print()">
    <h1>TOKO MAS JAMAL</h1>
    <p>Tanggal :{{$ambil->tanggal_beli}}</p>
    <p>Kasir : {{$kasir->name}}</p>
    
    <p>=======STRUK PEMBELIAN=======</p>
    <table style="margin-top:-10px">
    @foreach($transaksi as $u)
        <tr>
            <td style="padding-right:50px">Nama Barang : {{$u->nama_barang}}<br>
            </tr><tr>
            <td style="padding-right:50px">Jumlah Beli : {{$u->jumlah_beli}}</td>
            </tr><tr>
            <td style="padding-right:50px">Harga Barang : {{$u->harga_barang}}</td>
            </tr><tr>
            <td style="padding-right:50px">Total Harga : {{$u->total_harga}}</td>
        </tr>
    @endforeach
    
        <tr>
            <td colspan="3" style="text-align:right;padding-right:50px">Total Harga</td>
            <td>{{$jumlah}}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right;padding-right:50px">Bayar</td>
            <td>{{$kembalian->bayar}}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right;padding-right:50px">Kembalian</td>
            <td>{{$kembalian->kembalian}}</td>
        </tr>
    </table>
    <p>========TERIMAKASIH>========</p>
    
</body>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
